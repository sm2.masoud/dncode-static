function transformMenu() {
  $(".menu").css("transform", "translateX(100%)");

  $(".menu").css("display", "none");

  $(".menu__close").remove();
}

let videos = document.getElementsByTagName("video");

function checkScroll() {
  let fraction = 0.8; // Play when 80% of the player is visible.

  for (let i = 0; i < videos.length; i++) {
    let video = videos[i];

    let x = video.offsetLeft,
      y = video.offsetTop,
      w = video.offsetWidth,
      h = video.offsetHeight,
      r = x + w, //right
      b = y + h, //bottom
      visibleX,
      visibleY,
      visible;

    visibleX = Math.max(
      0,
      Math.min(
        w,
        window.pageXOffset + window.innerWidth - x,
        r - window.pageXOffset
      )
    );
    visibleY = Math.max(
      0,
      Math.min(
        h,
        window.pageYOffset + window.innerHeight - y,
        b - window.pageYOffset
      )
    );

    visible = (visibleX * visibleY) / (w * h);

    if (visible > fraction) {
      video.play();
    } else {
      video.pause();
    }
  }
}
function autoPlayVideo(x) {
  console.log(x, x.matches);

  if (x.matches) {
    window.addEventListener("scroll", checkScroll, false);
    window.addEventListener("resize", checkScroll, false);
  }
}
const x = window.matchMedia("(max-width: 62.5em)");
x.addListener(autoPlayVideo);
autoPlayVideo(x);
