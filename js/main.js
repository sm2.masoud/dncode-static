$(document).ready(function () {
  $(".menu__responsive").click(function (e) {
    $(".menu").prepend(
      '<span class="menu__close" onclick="transformMenu()"><i class="fas fa-times"></i></span>'
    );
    $(".menu").css("display", "grid");
    $(".menu").css("transform", "translateX(0)");
  });
  $(".post__item").hover(
    function () {
      const video = $(this).children("video");
      if (video.length > 0) {
        video.get(0).muted = true;
        video.get(0).play();
        $(this).find(".post__video--play").css("opacity", "0");
      }
    },
    function () {
      const video = $(this).children("video");
      if (video.length > 0) {
        $(this).children("video")[0].pause();
        $(this).find(".post__video--play").css("opacity", "1");
      }
    }
  );
  // $("video").each(function () {
  //   if ($(this).is(":in-viewport")) {
  //     $(this)[0].play();
  //   } else {
  //     $(this)[0].pause();
  //   }
  // });
});
